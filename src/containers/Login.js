import React, { Component } from 'react';
import "./Login.css";
import axios from 'axios';


class Login extends Component {
  login() {
    console.warn("state", this.state)
    axios.get('http://localhost:9090/user/getuser', this.state)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error)
      })


  }

  render() {
    return (

      <div className="Login">
        <h3>Login Page</h3>
        <div><label>Username:</label>
          <input type="text" placeholder="email"
            onChange={(e) => { this.setState({ email: e.target.value }) }}
          /><br /><br />
          <label>Password:</label>
          <input type="password" placeholder="password"
            onChange={(e) => { this.setState({ password: e.target.value }) }}
          /><br /><br />
          <button id="submit" onClick={() => this.login()}>Login</button>

        </div>
      </div>
    );
  }
}

export default Login;